using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipScript : MonoBehaviour
{
    float velocitat = 0.01f;
    public int score = 0;
    public GameObject bala;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("Score").GetComponent<TMPro.TMP_Text>().text = score.ToString();
        transform.position += new Vector3(Input.GetAxis("Horizontal") * velocitat, Input.GetAxis("Vertical") * velocitat, transform.position.z);
        if (Input.GetKeyDown("space")) if (bala!=null){ 
            Instantiate(bala, new Vector3(transform.position.x, transform.position.y + 1.5f, 0f), new Quaternion(180, 0, 0, 0));
        }
    }
}
