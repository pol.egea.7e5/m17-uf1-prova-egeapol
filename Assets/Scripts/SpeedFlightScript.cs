using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlightScript : MonoBehaviour
{
    float velocitat = 0.006f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0 , -1 * velocitat, 0);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "PlayerShip") {
            Destroy(collision);
            Destroy(gameObject);
        }
        if (collision.name == "fireball-blue-tail-small")
        {
            GameObject.Find("PlayerShip").GetComponent<PlayerShipScript>().score += 5;
            Destroy(collision);
            Destroy(gameObject);
        }
    }
}
